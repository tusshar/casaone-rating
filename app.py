from flask import Flask
app = Flask(__name__)

from catalogue.http_api import catalogue_api
app.register_blueprint(catalogue_api, url_prefix='/catalogue')
from ratings.http_api import rating_api
app.register_blueprint(rating_api, url_prefix='/ratings')

if __name__ == '__main__':
    app.run()
    