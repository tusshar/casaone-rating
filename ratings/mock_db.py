class MockResponse(object):

	def mockRatings(self, product_id):
		return {
			"5 stars" : 10,
			"4 stars" : 2,
			"3 stars" : 3,
			"2 stars" : 1,
			"1 stars" : 0
		}

