# Provides HTTP APIs for the catalogue model.

from flask import Blueprint, jsonify, current_app, request
from werkzeug.exceptions import BadRequest, NotFound, ExpectationFailed
from ratings.rating_service import RatingService

rating_api = Blueprint('rating_api', __name__)

rating_service = RatingService()

@rating_api.route('/product_rating/<product_id>')
def get_product_rating(product_id):
	
	mandatory_args = [product_id]
	if not all(mandatory_args):
		raise BadRequest('Mandatory args missing')

	try:
		product_rating = rating_service.get_product_rating(product_id)
		return jsonify(product_rating)
	except:
		raise ExpectationFailed("Something went wrong, try again!")
