from ratings.mock_db import MockResponse
from catalogue.catalogue_service import CatalogueService

catalogue_service = CatalogueService()
mock_response = MockResponse()

class RatingService(object):

	def __init__(self):
		pass
		

	def get_product_rating(self, product_id):
		"""
			Method used to get individual product rating.
			:param product_id: product id of catalogue
		    :type amount: int

		    :raises: :class:`RuntimeError`: something went wrong

		    :returns: average rating, count of 5 stars, count of 4 stars, \
		    	count of 3 stars, count of 2 stars, count of 1 stars.
		    :rtype: Dict
		"""

		avg_rating = catalogue_service.get_product(product_id)["average_rating"]

		# Get count of ratings from db GROUP BY stars
		# Mocking db response
		if avg_rating > 0:
			rating_depth = mock_response.mockRatings(product_id)
			rating_depth['average_rating'] = avg_rating
			
			return rating_depth
		else:
			return {}