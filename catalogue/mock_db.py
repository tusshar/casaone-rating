class MockResponse(object):

	def mockCatalogue(self):
		return [{
			"product_id": 12,
			"name": "Sofa",
			"type": "furniture",
			"category_id": 2,
			"average_rating" : 4 
		},
		{
			"product_id": 19,
			"name": "Bed",
			"type": "furniture",
			"category_id": 2,
			"average_rating" : 3
		}]