# Provides HTTP APIs for the catalogue model.

import json
from flask import Blueprint, current_app, request, jsonify
from werkzeug.exceptions import BadRequest, NotFound
from catalogue.catalogue_service import CatalogueService

catalogue_api = Blueprint('catalogue_api', __name__)

catalogue_service = CatalogueService()

@catalogue_api.route('/products')
def get_products():
    products = catalogue_service.get_products()
    return json.dumps(products)
    