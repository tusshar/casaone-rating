from catalogue.mock_db import MockResponse
from werkzeug.exceptions import NotFound

mock_response = MockResponse()

class CatalogueService(object):

	def __init__(self):
		pass

	def get_products(self):
		"""
		Get all products from db.
		Using mock response instead of actual db.
		"""
		products = mock_response.mockCatalogue()

		return products

	def get_product(self, product_id):
		"""
			returns dict of meta info for the given product_id 
		"""
		products = mock_response.mockCatalogue()

		for product in products:
			if product["product_id"] == int(product_id):
				return product

		raise NotFound("product not found")