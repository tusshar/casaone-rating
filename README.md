# Rating project

Flow diagram : [https://gitlab.com/tusshar/casaone-rating/blob/master/flowchart.png]

>Project is built in flask-python, http layer and service layer is defined.    
>Db models are not defined, instead mock response service is used to mock db response.

### Endpoints defined for web interface :
```
catalogue/products : returns all products in catalogue with average ratings
ratings/product/<product_id> : returns count of ratings group by stars.
```


